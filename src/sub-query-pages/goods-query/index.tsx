import { View } from "@tarojs/components";
import { AtSearchBar } from "taro-ui";
import { useState } from "react";
import Taro from "@tarojs/taro";
import "./index.scss";
export default function GoodsQuery() {
  const [queryKey, setQueryKey] = useState("");
  return (
    <View>
      <AtSearchBar
        onActionClick={() => {
          Taro.navigateTo({ url: `/sub-query-pages/goods-list/index?searchKey=${queryKey}` });
        }}
        focus={true}
        value={queryKey}
        placeholder="请输入搜索条件"
        onChange={(value) => {
          setQueryKey(value);
        }}
      />
    </View>
  );
}
