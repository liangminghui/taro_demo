import { View } from "@tarojs/components";
import { useState } from "react";
import { AtButton, AtDrawer, AtInput } from "taro-ui";
import { updateFilter } from "../../../store/features/goodsQueryFilterSlice";
import { useDispatch } from "react-redux";
import "./index.scss";
export default function GoodsFilterDrawer(props: any) {
  const [minPrice, setMinPrice] = useState("" as any);
  const [maxPrice, setMaxPrice] = useState("" as any);
  const dispatch = useDispatch();
  return (
    <View>
      <AtDrawer
        show={props.drawerShow}
        right
        mask
        onClose={() => {
          props.closeDrawer();
        }}
      >
        <View>
          <AtInput
            title="最低价"
            placeholder="请输入最低价"
            name="minPrice"
            type="number"
            value={minPrice}
            onChange={(value) => {
              setMinPrice(value);
            }}
          ></AtInput>
          <AtInput
            title="最高价"
            placeholder="请输入最高价"
            name="maxPrice"
            type="number"
            value={maxPrice}
            onChange={(value) => {
              setMaxPrice(value);
            }}
          ></AtInput>
          <AtButton
            type="primary"
            onClick={() => {
              dispatch(updateFilter({ minPrice, maxPrice }));
              props.closeDrawer();
            }}
          >
            筛选
          </AtButton>
        </View>
      </AtDrawer>
    </View>
  );
}
