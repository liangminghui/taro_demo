import { ScrollView, Text, View } from "@tarojs/components";
import { AtImagePicker, AtButton } from "taro-ui";
import { AtSearchBar } from "taro-ui";
import { useEffect, useState } from "react";
import "./index.scss";
import { getCurrentInstance } from "@tarojs/taro";
import { useSelector } from "react-redux";
import GoodsFilterDrawer from "./goods-filter-drawer";
export default function GoodsList() {
  const { router } = getCurrentInstance();
  const [queryKey, setQueryKey] = useState("");
  const [queryCateId, setQueryCateId] = useState("");
  const [data, setData] = useState([] as any);
  const [goodQueryIndex, setGoodsQueryIndex] = useState(1);
  const [drawerShow, setDrawerShow] = useState(false);
  const [bottomLoading, setBottomLoading] = useState({
    loading: false,
    hasMore: true,
  });
  const [filterSort, setFilterSort] = useState("");
  const { filterData } = useSelector((state: any) => state.goodsQueryFilter);
  useEffect(() => {
    const searchKey = router && router.params.searchKey;
    const categoryId = router && router.params.categoryId;
    if (searchKey) {
      setQueryKey(searchKey);
    }
    if (categoryId) {
      setQueryCateId(categoryId);
    }
    if (goodQueryIndex > 5) {
      setBottomLoading({ loading: false, hasMore: false });
      return;
    }
    setBottomLoading({ loading: true, hasMore: true });
    const timer = setTimeout(() => {
      clearTimeout(timer);
      const loadData = [
        {
          id: 111 + "" + goodQueryIndex,
          photo: "",
          title: `第${goodQueryIndex}页,第一个商品`,
          price: goodQueryIndex * 100,
        },
        {
          id: 222 + "" + goodQueryIndex,
          photo: "",
          title: `第${goodQueryIndex}页,第二个商品`,
          price: goodQueryIndex * 200,
        },
        {
          id: 333 + "" + goodQueryIndex,
          photo: "",
          title: `第${goodQueryIndex}页,第三个商品`,
          price: goodQueryIndex * 300,
        },
        {
          id: 444 + "" + goodQueryIndex,
          photo: "",
          title: `第${goodQueryIndex}页,第四个商品`,
          price: goodQueryIndex * 400,
        },
      ];
      setData((data) => [...data, ...loadData]);
      setBottomLoading({ loading: false, hasMore: true });
    }, 2000);
  }, []);
  return (
    <ScrollView
      className="goods-list"
      scrollY={true}
      onScrollToLower={() => {
        setGoodsQueryIndex(goodQueryIndex + 1);
      }}
    >
      <AtSearchBar
        onActionClick={() => {
          console.log(
            "goodQueryIndex",
            goodQueryIndex,
            "queryKey",
            queryKey,
            "queryCateId",
            queryCateId,
            "filterSort",
            filterSort,
            "filterData",
            filterData
          );
        }}
        focus={true}
        value={queryKey}
        placeholder="请输入搜索条件"
        onChange={(value) => {
          setQueryKey(value);
        }}
      />
      <View>
        <AtButton
          size="small"
          type={filterSort === "generate" ? "primary" : undefined}
          className="filter-btn"
          onClick={() => {
            setFilterSort("generate");
          }}
        >
          综合
        </AtButton>
        <AtButton
          size="small"
          className="filter-btn"
          type={filterSort === "selelNum" ? "primary" : undefined}
          onClick={() => {
            setFilterSort("selelNum");
          }}
        >
          销量
        </AtButton>
        <AtButton
          size="small"
          type={filterSort === "price" ? "primary" : undefined}
          className="filter-btn"
          onClick={() => {
            setFilterSort("price");
          }}
        >
          价格
        </AtButton>
        <AtButton
          size="small"
          type={filterSort === "price" ? "primary" : undefined}
          className="filter-btn"
          onClick={() => {
            setDrawerShow(true);
          }}
        >
          筛选
        </AtButton>
        <GoodsFilterDrawer
          drawerShow={drawerShow}
          closeDrawer={() => setDrawerShow(false)}
        ></GoodsFilterDrawer>
      </View>
      <View>
        {data.map((item) => (
          <View>
            <AtImagePicker
              files={[
                {
                  url: "https://storage.360buyimg.com/mtd/home/111543234387022.jpg",
                },
              ]}
              showAddBtn={false}
              onImageClick={() => {
                console.log(item["id"]);
              }}
              count={1}
              onChange={() => {}}
              sizeType={["original", "compressed"]}
            />
            <Text>
              {item["title"]}:{(item["price"] / 100).toFixed(2)}
            </Text>
          </View>
        ))}
      </View>
      {bottomLoading.loading ? (
        <Text>加载中</Text>
      ) : bottomLoading.hasMore ? (
        ""
      ) : (
        <Text>没有更多了</Text>
      )}
    </ScrollView>
  );
}
