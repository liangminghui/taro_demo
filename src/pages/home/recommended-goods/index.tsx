import { View, Text } from "@tarojs/components";
import { useEffect, useState } from "react";
import { AtImagePicker } from "taro-ui";
import Taro from "@tarojs/taro";
import "./index.scss";
export default function RecommendedGoods(props: any) {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([] as any);
  useEffect(() => {
    if (props.queryIndex > 5) {
      props.bottomLoadingCallBack({ loading: false, hasMore: false });
      return;
    }
    props.bottomLoadingCallBack({ loading: true, hasMore: true });
    const timer = setTimeout(() => {
      clearTimeout(timer);
      setLoading(false);
      const loadData = [
        {
          id: 111 + "" + props.queryIndex,
          photo: "",
          title: `第${props.queryIndex}页,第一个商品`,
          price: props.queryIndex * 100,
        },
        {
          id: 222 + "" + props.queryIndex,
          photo: "",
          title: `第${props.queryIndex}页,第二个商品`,
          price: props.queryIndex * 200,
        },
        {
          id: 333 + "" + props.queryIndex,
          photo: "",
          title: `第${props.queryIndex}页,第三个商品`,
          price: props.queryIndex * 300,
        },
        {
          id: 444 + "" + props.queryIndex,
          photo: "",
          title: `第${props.queryIndex}页,第四个商品`,
          price: props.queryIndex * 400,
        },
      ];
      setData((data) => [...data, ...loadData]);
      props.bottomLoadingCallBack({ loading: false, hasMore: true });
    }, 500);
  }, [props.queryIndex]);

  return (
    <View className="recommended-goods">
      {loading ? (
        ""
      ) : (
        <View>
          <View>
            {data.map((item) => (
              <View
                onClick={() =>
                  Taro.redirectTo({
                    url: `/sub-detail-pages/goods-detail/index?goodsId=${item.id}`,
                  })
                }
              >
                <AtImagePicker
                  files={[
                    {
                      url: "https://storage.360buyimg.com/mtd/home/111543234387022.jpg",
                    },
                  ]}
                  showAddBtn={false}
                  onImageClick={() => {
                    console.log(123);
                  }}
                  count={1}
                  onChange={() => {}}
                  sizeType={["original", "compressed"]}
                />
                <Text>
                  {item["title"]}:{(item["price"] / 100).toFixed(2)}
                </Text>
              </View>
            ))}
          </View>
        </View>
      )}
    </View>
  );
}
