import { View, Text } from "@tarojs/components";
import { useEffect, useState } from "react";
import "./index.scss";
export default function ResidentActivities() {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([] as any);
  useEffect(() => {
    const timer = setTimeout(() => {
      clearTimeout(timer);
      setLoading(false);
      setData([
        {
          title: "秒杀活动",
          goodsData: [
            { id: "111", photo: "", price: 100 },
            { id: "222", photo: "", price: 200 },
          ],
        },
        {
          title: "官方补贴",
          goodsData: [
            { id: "333", photo: "", price: 300 },
            { id: "444", photo: "", price: 400 },
          ],
        },
      ]);
    }, 2000);
  }, []);

  return (
    <View className="resident-activities">
      {loading ? (
        <Text>加载中</Text>
      ) : data.length >= 2 ? (
        <View>
          <View>
            <Text>{data[0].title}</Text>
            {data[0].goodsData.map((item) => (
              <View
                onClick={() => {
                  console.log(item.id);
                }}
              >
                <Text>{(item.price / 100).toFixed(2)}</Text>
              </View>
            ))}
          </View>
          <View>
            <Text>{data[1].title}</Text>
            {data[1].goodsData.map((item) => (
              <View
                onClick={() => {
                  console.log(item.id);
                }}
              >
                <Text>{(item.price / 100).toFixed(2)}</Text>
              </View>
            ))}
          </View>
        </View>
      ) : data.length === 1 ? (
        <View>
          <View>
            <Text>{data[0].title}</Text>
            {data[0].goodsData.map((item) => (
              <View
                onClick={() => {
                  console.log(item.id);
                }}
              >
                <Text>{(item.price / 100).toFixed(2)}</Text>
              </View>
            ))}
          </View>
        </View>
      ) : (
        ""
      )}
    </View>
  );
}
