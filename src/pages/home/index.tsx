import { ScrollView, Text } from "@tarojs/components";
import { useState } from "react";
import "./index.scss";
import RecommendedGoods from "./recommended-goods";
import ResidentActivities from "./resident-activities";
import { AtSearchBar } from "taro-ui";
import Taro from "@tarojs/taro";

export default function Home() {
  const [goodQueryIndex, setGoodsQueryIndex] = useState(1);
  const [bottomLoading, setBottomLoading] = useState({
    loading: false,
    hasMore: true,
  });
  return (
    <ScrollView
      className="home"
      scrollY={true}
      onScrollToLower={() => {
        setGoodsQueryIndex(goodQueryIndex + 1);
      }}
    >
      <AtSearchBar
        value={""}
        onChange={() => {}}
        onFocus={() => {
          Taro.navigateTo({ url: "/sub-query-pages/goods-query/index" });
        }}
      />
      <ResidentActivities />
      <RecommendedGoods
        queryIndex={goodQueryIndex}
        bottomLoadingCallBack={(value) => setBottomLoading(value)}
      />
      {bottomLoading.loading ? (
        <Text>加载中</Text>
      ) : bottomLoading.hasMore ? (
        ""
      ) : (
        <Text>没有更多了</Text>
      )}
    </ScrollView>
  );
}
