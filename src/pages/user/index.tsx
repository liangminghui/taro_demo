import { Text, View } from "@tarojs/components";
import Taro from "@tarojs/taro";
import { useState } from "react";
import { AtAvatar } from "taro-ui";
import "./index.scss";
export default function User() {
  const [userInfo, setUserInfo] = useState({ name: "测试名称" });
  const [balanceInfo, setBalanceInfo] = useState({
    redCart: 999.87,
    coupon: 13,
    collect: 25,
    visitCount: 14,
  });
  return (
    <View className="user">
      <View>
        <Text>{userInfo ? userInfo["name"] : "暂无"}</Text>
      </View>
      <View>
        <Text>红包{balanceInfo.redCart}元</Text>
        <Text>优惠券{balanceInfo.coupon}张</Text>
        <Text>收藏{balanceInfo.collect}</Text>
        <Text>足迹{balanceInfo.visitCount}</Text>
      </View>
      <View>
        <Text>我的订单</Text>
        <Text>查看所有订单</Text>
        <View>
          <View
            className="atavatar"
            onClick={() => {
              Taro.redirectTo({
                url: "/sub-query-pages/order-list/index?tabKey=1",
              });
            }}
          >
            <AtAvatar image="https://jdc.jd.com/img/200"></AtAvatar>
            待付款
          </View>
          <View
            className="atavatar"
            onClick={() => {
              Taro.redirectTo({
                url: "/sub-query-pages/order-list/index?tabKey=2",
              });
            }}
          >
            <AtAvatar image="https://jdc.jd.com/img/200"> </AtAvatar>
            待发货
          </View>
          <View
            className="atavatar"
            onClick={() => {
              Taro.redirectTo({
                url: "/sub-query-pages/order-list/index?tabKey=3",
              });
            }}
          >
            <AtAvatar image="https://jdc.jd.com/img/200"></AtAvatar>
            待收货
          </View>
          <View className="atavatar">
            <AtAvatar image="https://jdc.jd.com/img/200"></AtAvatar>
            退换/售后
          </View>
        </View>
      </View>
      <View>
        <Text>我的服务</Text>
        <View>
          <View className="atavatar">
            <AtAvatar image="https://jdc.jd.com/img/200"></AtAvatar>
            客户服务
          </View>
          <View className="atavatar">
            <AtAvatar image="https://jdc.jd.com/img/200"></AtAvatar>
            我的钱包
          </View>
          <View className="atavatar">
            <AtAvatar image="https://jdc.jd.com/img/200"></AtAvatar>
            投诉建议
          </View>
          <View className="atavatar">
            <AtAvatar image="https://jdc.jd.com/img/200"></AtAvatar>
            在线客服
          </View>
        </View>
      </View>
    </View>
  );
}
