import { View, Text } from "@tarojs/components";
import { useEffect, useState } from "react";
import ShopCartItem from "./shop-cart-item";
import ShopCartEdit from "./shop-cart-edit";
import { AtButton, AtDivider } from "taro-ui";
import { AtSwipeAction } from "taro-ui";
import { AtCheckbox } from "taro-ui";
import "./index.scss";
import Taro from "@tarojs/taro";
export default function ShopCart() {
  const [shopCartList, setShopCartList] = useState([] as any);
  useEffect(() => {
    let cartList = [
      {
        shopId: 1,
        shopName: "店铺A",
        isChecked: false,
        shopList: [
          {
            id: "1",
            isChecked: false,
            name: "第1个商品",
            price: 100,
            num: 1,
          },
          {
            id: "2",
            isChecked: false,
            name: "第2个商品",
            price: 200,
            num: 2,
          },
        ],
      },
      {
        shopId: 2,
        shopName: "店铺B",
        isChecked: false,
        shopList: [
          {
            id: "3",
            isChecked: false,
            name: "第3个商品",
            price: 300,
            num: 3,
          },
          {
            id: "4",
            isChecked: false,
            name: "第4个商品",
            price: 400,
            num: 4,
          },
        ],
      },
    ];
    setShopCartList(cartList);
  }, []);
  const addItemNum = (id: string, num: number) => {
    const newShopCartList = [] as any;
    shopCartList.map((item) => {
      let curItem = item["shopList"].map((cart) => {
        if (cart.id === id) {
          cart.num += num;
        }
        return cart;
      });
      newShopCartList.push({
        isChecked: item.isChecked,
        shopId: item["shopId"],
        shopName: item["shopName"],
        shopList: curItem,
      });
    });
    setShopCartList(newShopCartList);
  };
  const changeShopChecked = (shopId) => {
    const newShopCartList = [] as any;
    shopCartList.map((item) => {
      if (item.shopId === shopId) {
        item.isChecked = !item.isChecked;
      }
      item["shopList"].map((cart) => {
        cart.isChecked = item.isChecked;
      });
      newShopCartList.push({
        isChecked: item.isChecked,
        shopId: item["shopId"],
        shopName: item["shopName"],
        shopList: item["shopList"],
      });
    });
    setShopCartList(newShopCartList);
    calcuate();
  };
  const changeCartChecked = (id) => {
    const newShopCartList = [] as any;
    shopCartList.map((item) => {
      item["shopList"].map((cart) => {
        if (cart.id === id) {
          cart.isChecked = !cart.isChecked;
        }
      });
      let shopCaheckedFlag =
        item["shopList"].filter((item) => !item.isChecked).length === 0;
      console.log(shopCaheckedFlag);
      newShopCartList.push({
        isChecked: shopCaheckedFlag,
        shopId: item["shopId"],
        shopName: item["shopName"],
        shopList: item["shopList"],
      });
    });
    setShopCartList(newShopCartList);
    calcuate();
  };
  const calcuate = () => {
    let totalPrice = 0;
    shopCartList.map((item) => {
      item["shopList"].map((cart) => {
        if (cart.isChecked) {
          totalPrice += cart.price * cart.num;
        }
      });
    });
    //console.log(totalPrice);
  };
  return (
    <View className="shop-cart">
      {shopCartList.map((shopItem) => (
        <View>
          <Text className="shop-title">{shopItem["shopName"]}</Text>
          <AtCheckbox
            options={[{ value: shopItem["id"], label: "" }]}
            selectedList={shopItem["isChecked"] ? [shopItem["id"]] : []}
            onChange={() => {
              changeShopChecked(shopItem["shopId"]);
            }}
          />
          {shopItem["shopList"].map((item) => (
            <View>
              <AtCheckbox
                options={[{ value: item["id"] + "_item", label: "" }]}
                selectedList={item["isChecked"] ? [item["id"] + "_item"] : []}
                onChange={() => {
                  changeCartChecked(item["id"]);
                }}
              />
              <AtSwipeAction maxDistance={150} areaWidth={350}>
                <ShopCartItem item={item}></ShopCartItem>
                <ShopCartEdit
                  item={item}
                  addIndexNum={addItemNum}
                ></ShopCartEdit>
                <AtButton type="primary">点我删除</AtButton>
                <AtButton type="primary">移入收藏夹</AtButton>
              </AtSwipeAction>
            </View>
          ))}
          <AtDivider content="" />
          <AtButton
            onClick={() => {
              let cartIds = [] as any;
              shopCartList.map((item) => {
                item["shopList"].map((cart) => {
                  if (cart.isChecked) {
                    cartIds.push(cart.id);
                  }
                });
              });
              Taro.redirectTo({
                url:
                  "/sub-detail-pages/confirm-order/index?urlType=shopCart&cartIds=" +
                  JSON.stringify(cartIds),
              });
            }}
          >
            去结算
          </AtButton>
        </View>
      ))}
    </View>
  );
}
