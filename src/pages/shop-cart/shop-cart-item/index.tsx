import { View, Text } from "@tarojs/components";
import { Component } from "react";
import "./index.scss";
class ShopCartItem extends Component<any> {
  constructor(props) {
    super(props);
  }
  render() {
    const item = this.props.item;
    return (
      <View className="shop-cart-item">
        <Text>商品名称{item.name},数量{item.num}</Text>
      </View>
    );
  }
}

export default ShopCartItem;
