import { View } from "@tarojs/components";
import { Component } from "react";
import { AtButton } from "taro-ui";
import "./index.scss";
class ShopCartEdit extends Component<any> {
  constructor(props) {
    super(props);
  }
  render() {
    const item = this.props.item;
    return (
      <View className="shop-cart-edit">
        <AtButton onClick={() => this.props.addIndexNum(item["id"], 1)}>
          点我{item["name"]}+1
        </AtButton>
        <AtButton onClick={() => this.props.addIndexNum(item["id"], -1)}>
          点我{item["name"]}-1
        </AtButton>
      </View>
    );
  }
}

export default ShopCartEdit;
