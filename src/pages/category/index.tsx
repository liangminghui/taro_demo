import { View } from "@tarojs/components";
import "./index.scss";
import Taro from "@tarojs/taro";
import { AtTabs, AtTabsPane, AtSearchBar } from "taro-ui";
import { useState } from "react";

export default function Category() {
  const firstCategoryArr = [
    { id: 1, title: "美食" },
    { id: 2, title: "电影" },
    { id: 3, title: "酒店" },
    { id: 4, title: "休闲娱乐" },
    { id: 5, title: "外卖" },
    { id: 6, title: "超市" },
    { id: 7, title: "KTV" },
    { id: 8, title: "周边游" },
  ];
  const secondCategoryArr = [
    { pId: 1, id: 111, title: "美食1" },
    { pId: 1, id: 112, title: "美食2" },
    { pId: 1, id: 113, title: "美食3" },
    { pId: 2, id: 211, title: "电影1" },
    { pId: 2, id: 212, title: "电影2" },
    { pId: 2, id: 213, title: "电影3" },
    { pId: 3, id: 311, title: "酒店1" },
    { pId: 3, id: 312, title: "酒店2" },
    { pId: 3, id: 313, title: "酒店3" },
    { pId: 4, id: 411, title: "休闲娱乐1" },
    { pId: 4, id: 412, title: "休闲娱乐2" },
    { pId: 4, id: 413, title: "休闲娱乐3" },
    { pId: 5, id: 511, title: "外卖1" },
    { pId: 5, id: 512, title: "外卖2" },
    { pId: 5, id: 513, title: "外卖3" },
  ];
  const [currentFirstCate, setCurrentFirstCate] = useState(0);
  return (
    <View className="category">
      <AtSearchBar
        value={""}
        onChange={() => {}}
        onFocus={() => {
          Taro.navigateTo({ url: "/sub-query-pages/goods-query/index" });
        }}
      />
      <AtTabs
        current={currentFirstCate}
        scroll
        tabDirection="vertical"
        tabList={firstCategoryArr}
        onClick={(value) => {
          setCurrentFirstCate(value);
        }}
      >
        {firstCategoryArr.map((firstCate, index) => (
          <AtTabsPane
            tabDirection="vertical"
            current={index}
            index={index}
            className="category-item"
          >
            {index === currentFirstCate
              ? secondCategoryArr.map((secondCate) =>
                  secondCate.pId === firstCate.id ? (
                    <View
                      onClick={() =>
                        Taro.navigateTo({
                          url: `/sub-query-pages/goods-list/index?categoryId=${secondCate.id}`,
                        })
                      }
                    >
                      标签页一的内容{currentFirstCate}
                      {secondCate.title}
                    </View>
                  ) : null
                )
              : null}
          </AtTabsPane>
        ))}
      </AtTabs>
    </View>
  );
}
