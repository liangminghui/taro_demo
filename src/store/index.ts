import { configureStore } from "@reduxjs/toolkit";
import goodsQueryFilterSlice from "./features/goodsQueryFilterSlice";
import goodsDetailProductSlice from "./features/goodsDetailProductSlice";
// configureStore创建一个redux数据
export default configureStore({
  reducer: {
    goodsQueryFilter: goodsQueryFilterSlice,
    goodsDetailProduct: goodsDetailProductSlice,
  },
});
