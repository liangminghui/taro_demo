import { createSlice } from "@reduxjs/toolkit";
export const goodsDetailProductSlice = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action的前缀
  name: "goodsDetailProduct",
  // 初始值
  initialState: {
    goodsDetailProduct: {
      goodsId: "",
      desc: "",
      photos: [],
    },
  },
  // 这里的属性会自动的导出为actions，在组件中可以直接通过dispatch进行触发
  reducers: {
    updateGoodsDetailProduct(state, { payload }) {
      // 内置了immutable
      state.goodsDetailProduct = payload;
    },
  },
});

// 导出actions
export const { updateGoodsDetailProduct } = goodsDetailProductSlice.actions;
// 导出reducer，在创建store时使用到
export default goodsDetailProductSlice.reducer;
