export default defineAppConfig({
  pages: [
    "pages/home/index",
    "pages/category/index",
    "pages/shop-cart/index",
    "pages/user/index",
  ],
  //分包配置
  subPackages: [
    {
      root: "sub-query-pages",
      name: "query-pages",
      pages: ["goods-query/index", "goods-list/index", "order-list/index"],
    },
    {
      root: "sub-detail-pages",
      name: "detail-pages",
      pages: ["goods-detail/index", "confirm-order/index"],
    },
  ],
  tabBar: {
    color: "#333",
    selectedColor: "#ff0000",
    backgroundColor: "#fff",
    borderStyle: "black",
    list: [
      {
        pagePath: "pages/home/index",
        text: "首页",
        iconPath: "static/images/tab-bar/home.png",
        selectedIconPath: "static/images/tab-bar/home-active.png",
      },
      {
        pagePath: "pages/category/index",
        text: "分类",
        iconPath: "static/images/tab-bar/category.png",
        selectedIconPath: "static/images/tab-bar/category-active.png",
      },
      {
        pagePath: "pages/shop-cart/index",
        text: "购物车",
        iconPath: "static/images/tab-bar/shop-cart.png",
        selectedIconPath: "static/images/tab-bar/shop-cart-active.png",
      },
      {
        pagePath: "pages/user/index",
        text: "我的",
        iconPath: "static/images/tab-bar/user.png",
        selectedIconPath: "static/images/tab-bar/user-active.png",
      },
    ],
  },
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black",
  },
});
