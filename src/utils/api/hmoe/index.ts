import { get, post, put, del } from "../../request";
const homeReqUrl = `${process.env.HOME_REQ_URL}/index`;
export const testGet = () => get(homeReqUrl);
export const testPost = (data) => post(homeReqUrl, data);
export const testPut = (data) => put(homeReqUrl, data);
export const testDel = (id) => del(`${homeReqUrl}/${id}`);
