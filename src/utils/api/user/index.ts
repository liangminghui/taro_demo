import { post } from "../../request";
const homeReqUrl = `${process.env.USER_REQ_URL}`;
export const loginByCode = (code) =>
  post(`${homeReqUrl}/auth/login?code=${code}`, null);
export const test404 = (code) =>
  post(`${homeReqUrl}/auth/login/aaaBBBccc?code=${code}`, null);
  export const test403 = (code) =>
  post(`${homeReqUrl}/auth/403?code=${code}`, null);
  export const test401 = (code) =>
  post(`${homeReqUrl}/auth/401?code=${code}`, null);
  export const test500 = (code) =>
  post(`${homeReqUrl}/auth/error?code=${code}`, null);
  export const test10001 = (code) =>
  post(`${homeReqUrl}/auth/10001?code=${code}`, null);