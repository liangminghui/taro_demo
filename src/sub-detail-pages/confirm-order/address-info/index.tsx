import { View } from "@tarojs/components";
import { AtButton } from "taro-ui";
import './index.scss';
export default function AddressInfo(props) {
  const updateAddressId = () => {
    let addressId = new Date().getTime();
    props.updateAddressId(addressId);
  };
  return (
    <View>
      <AtButton onClick={() => updateAddressId()}>
        点我覆盖地址唯一标识
      </AtButton>
    </View>
  );
}
