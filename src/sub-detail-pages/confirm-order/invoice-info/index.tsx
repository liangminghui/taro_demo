import { View } from "@tarojs/components";
import { AtButton } from "taro-ui";
import "./index.scss";
export default function InvoiceInfo(props) {
  const updateInvoiceId = () => {
    let invoiceId = new Date().getTime();
    props.updateInvoiceId(invoiceId);
  };
  return (
    <View>
      <AtButton onClick={() => updateInvoiceId()}>
        点我覆盖地址优惠券唯一标识
      </AtButton>
    </View>
  );
}
