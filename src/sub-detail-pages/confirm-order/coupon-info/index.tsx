import { View } from "@tarojs/components";
import { AtButton } from "taro-ui";
import "./index.scss";
export default function CouponInfo(props) {
  const updateCouponId = () => {
    let couponId = new Date().getTime();
    props.updateCouponId(couponId);
  };
  return (
    <View>
      <AtButton onClick={() => updateCouponId()}>
        点我覆盖地址优惠券唯一标识
      </AtButton>
    </View>
  );
}
