import { View } from "@tarojs/components";
import AddressInfo from "./address-info";
import CouponInfo from "./coupon-info";
import InvoiceInfo from "./invoice-info";
import { getCurrentInstance } from "@tarojs/taro";
import { useEffect, useState } from "react";

export default function ConfirmOrder() {
  const { router } = getCurrentInstance() || {};
  const [loading, setLoading] = useState(true);
  const [addressId, setAddressId] = useState("1111");
  const [couponId, setCouponId] = useState("2222");
  const [invoiceId, setInvoiceId] = useState("3333");
  const [confirmOrderData, setConfirmOrderData] = useState({
    shopData: [
      {
        shopName: "测试店铺",
        goodList: [
          {
            goodsId: "123123123",
            goodsName: "商品名称",
            goodsPrice: "123",
            goodsNum: "1",
          },
          {
            goodsId: "123123123",
            goodsName: "商品名称",
            goodsPrice: "123",
            goodsNum: "1",
          },
        ],
      },
    ],
  });
  useEffect(() => {
    console.log(router?.params);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);
  useEffect(() => {
    console.log(addressId);
    console.log(couponId);
    console.log(invoiceId);
    console.log("计算");
  }, [addressId, couponId, invoiceId]);
  return (
    <View>
      {loading ? (
        "加载中"
      ) : (
        <View>
          {addressId} {couponId} {invoiceId}
          <AddressInfo
            addressId={addressId}
            updateAddressId={setAddressId}
          ></AddressInfo>
          <CouponInfo
            couponId={couponId}
            updateCouponId={setCouponId}
          ></CouponInfo>
          <InvoiceInfo
            invoiceId={invoiceId}
            updateInvoiceId={setInvoiceId}
          ></InvoiceInfo>
        </View>
      )}
    </View>
  );
}
