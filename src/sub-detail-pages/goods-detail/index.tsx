import { View, ScrollView } from "@tarojs/components";
import "./index.scss";
import ProductPhoto from "./product-photo";
import ProductProps from "./product-props";
import ProductSelector from "./product-selector";
import { getCurrentInstance } from "@tarojs/taro";
import { updateGoodsDetailProduct } from "../../store/features/goodsDetailProductSlice";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { AtDivider } from "taro-ui";

import { AtButton } from "taro-ui";
import Taro from "@tarojs/taro";
export default function GoodsDetail() {
  const { router } = getCurrentInstance();
  const { goodsDetailProduct } = useSelector(
    (state: any) => state.goodsDetailProduct
  );
  const dispatch = useDispatch();
  useEffect(() => {
    const goodsId = router && router.params.goodsId;
    dispatch(
      updateGoodsDetailProduct({
        goodsId: goodsId,
        desc: `<p1>商品id——:${goodsId}——的描述</p1>`,
        name: `${goodsId}商品名称`,
        price: `${goodsId}商品价格`,
        productStr: `${goodsId}的规格描述，内存，颜色等`,
        photos: [
          "https://storage.360buyimg.com/mtd/home/111543234387022.jpg",
          "https://storage.360buyimg.com/mtd/home/221543234387016.jpg",
          "https://storage.360buyimg.com/mtd/home/111543234387022.jpg",
          "https://storage.360buyimg.com/mtd/home/111543234387022.jpg",
        ],
      })
    );
  }, []);
  return (
    <View className="goodsDetail">
      <ScrollView scrollY={true} className="goods_detail_scroll">
        <ProductPhoto />
        <AtDivider content="" />
        <ProductProps />
        <AtDivider content="" />
        <ProductSelector />
        <AtDivider content="" />
        <View
          dangerouslySetInnerHTML={{ __html: goodsDetailProduct.desc }}
        ></View>
      </ScrollView>
      <View>
        <AtButton type="primary">加入购物车</AtButton>
        <AtButton
          type="secondary"
          onClick={() =>
            Taro.redirectTo({
              url: `/sub-detail-pages/confirm-order/index?urlType=buyNow&productId='123123123'&buyNum=10`,
            })
          }
        >
          立即购买
        </AtButton>
      </View>
    </View>
  );
}
