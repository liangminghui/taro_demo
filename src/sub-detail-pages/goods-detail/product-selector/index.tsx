import { View, Text } from "@tarojs/components";
import "./index.scss";
import { useSelector, useDispatch } from "react-redux";
import { AtButton, AtFloatLayout } from "taro-ui";
import { useState } from "react";
import { AtList, AtListItem } from "taro-ui";
import { updateGoodsDetailProduct } from "../../../store/features/goodsDetailProductSlice";

export default function ProductSelector() {
  const { goodsDetailProduct } = useSelector(
    (state: any) => state.goodsDetailProduct
  );
  const [showChooseFlag, setShowChooseFlag] = useState(false);
  const dispatch = useDispatch();
  const bindProdcut = (goodsId) => {
    dispatch(
      updateGoodsDetailProduct({
        goodsId: goodsId,
        desc: `<p1>商品id——:${goodsId}——的描述</p1>`,
        name: `${goodsId}商品名称`,
        price: `${goodsId}商品价格`,
        productStr: `${goodsId}的规格描述，内存，颜色等`,
        photos: [
          "https://storage.360buyimg.com/mtd/home/111543234387022.jpg",
          "https://storage.360buyimg.com/mtd/home/221543234387016.jpg",
          "https://storage.360buyimg.com/mtd/home/111543234387022.jpg",
          "https://storage.360buyimg.com/mtd/home/111543234387022.jpg",
        ],
      })
    );
  };
  return (
    <View className="goods-selector">
      <View className="selector-prodcut">
        已选择:<Text>{goodsDetailProduct.productStr}</Text>
        <AtButton
          onClick={() => setShowChooseFlag(true)}
          type="primary"
          size="small"
        >
          点我切换
        </AtButton>
        <AtFloatLayout
          isOpened={showChooseFlag}
          title=""
          onClose={() => setShowChooseFlag(false)}
        >
          <AtList>
            <AtListItem
              title="点我选择规格1111"
              onClick={() => bindProdcut(1111)}
            />
            <AtListItem
              title="点我选择规格2222"
              onClick={() => bindProdcut(2222)}
            />
            <AtListItem
              title="点我选择规格3333"
              onClick={() => bindProdcut(3333)}
            />
            <AtListItem
              title="点我选择规格4444"
              onClick={() => bindProdcut(4444)}
            />
          </AtList>
        </AtFloatLayout>
      </View>
    </View>
  );
}
