import { View } from "@tarojs/components";
import "./index.scss";
import { useSelector } from "react-redux";
import { Swiper, SwiperItem } from "@tarojs/components";
import { AtImagePicker } from "taro-ui";
export default function ProductPhoto() {
  const { goodsDetailProduct } = useSelector(
    (state: any) => state.goodsDetailProduct
  );
  return (
    <View>
      {goodsDetailProduct.photos && goodsDetailProduct.photos.length > 0 ? (
        <Swiper
          className="test-h"
          indicatorColor="#999"
          indicatorActiveColor="#333"
          circular
          autoplay
          indicatorDots
        >
          <SwiperItem>
            <View>
              <AtImagePicker
                files={[
                  {
                    url: goodsDetailProduct.photos[0],
                  },
                ]}
                showAddBtn={false}
                onImageClick={() => {}}
                count={1}
                onChange={() => {}}
                sizeType={["original", "compressed"]}
              />
            </View>
          </SwiperItem>
          <SwiperItem>
            <View>
              <AtImagePicker
                files={[
                  {
                    url: goodsDetailProduct.photos[1],
                  },
                ]}
                showAddBtn={false}
                onImageClick={() => {}}
                count={1}
                onChange={() => {}}
                sizeType={["original", "compressed"]}
              />
            </View>
          </SwiperItem>
          <SwiperItem>
            <View>
              <AtImagePicker
                files={[
                  {
                    url: goodsDetailProduct.photos[2],
                  },
                ]}
                showAddBtn={false}
                onImageClick={() => {}}
                count={1}
                onChange={() => {}}
                sizeType={["original", "compressed"]}
              />
            </View>
          </SwiperItem>
        </Swiper>
      ) : null}
    </View>
  );
}
