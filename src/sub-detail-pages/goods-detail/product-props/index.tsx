import { View, Text } from "@tarojs/components";
import "./index.scss";
import { useSelector } from "react-redux";

export default function ProductProps() {
  const { goodsDetailProduct } = useSelector(
    (state: any) => state.goodsDetailProduct
  );
  return (
    <View className="goods-props">
      <View>
        <Text>{goodsDetailProduct.name}</Text>
      </View>
      <View className="price">
        <Text>{goodsDetailProduct.price}</Text>
      </View>
    </View>
  );
}
